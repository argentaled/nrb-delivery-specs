var infopack = require('infopack');
var fs = require('fs');

var pipeline = new infopack.Pipeline();

pipeline.addStep({
    run: function(pipeline) {
        pipeline.writeFile({
            title: 'Leveransspecifikation - Objekt - Informationsmängder - Programhandling (PDF)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-objekt-programhandling-ph.pdf',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-objekt-programhandling-ph.pdf')
        });
        pipeline.writeFile({
            title: 'Leveransspecifikation - Objekt - Informationsmängder - Programhandling (MS Excel)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-objekt-programhandling-ph.xlsx',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-objekt-programhandling-ph.xlsx')
        });
    }
});

pipeline.addStep({
    run: function(pipeline) {
        pipeline.writeFile({
            title: 'Leveransspecifikation - Objekt - Informationsmängder - Systemhandling (PDF)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-objekt-systemhandling-sh.pdf',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-objekt-systemhandling-sh.pdf')
        });
        pipeline.writeFile({
            title: 'Leveransspecifikation - Objekt - Informationsmängder - Systemhandling (MS Excel)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-objekt-systemhandling-sh.xlsx',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-objekt-systemhandling-sh.xlsx')
        });
    }
});

pipeline.addStep({
    run: function(pipeline) {
        pipeline.writeFile({
            title: 'Leveransspecifikation - Objekt - Informationsmängder - Bygghandling (PDF)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-objekt-bygghandling-bh.pdf',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-objekt-bygghandling-bh.pdf')
        });
        pipeline.writeFile({
            title: 'Leveransspecifikation - Objekt - Informationsmängder - Bygghandling (MS Excel)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-objekt-bygghandling-bh.xlsx',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-objekt-bygghandling-bh.xlsx')
        });
    }
});

pipeline.addStep({
    run: function(pipeline) {
        pipeline.writeFile({
            title: 'Leveransspecifikation - Objekt - Informationsmängder - Relationshandling (PDF)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-objekt-relationshandling-rh.pdf',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-objekt-relationshandling-rh.pdf')
        });
        pipeline.writeFile({
            title: 'Leveransspecifikation - Objekt - Informationsmängder - Relationshandling (MS Excel)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-objekt-relationshandling-rh.xlsx',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-objekt-relationshandling-rh.xlsx')
        });
    }
});

pipeline.addStep({
    run: function(pipeline) {
        pipeline.writeFile({
            title: 'Leveransspecifikation - Dokument - Informationsmängder - Planering (MS Excel)',
            description: 'Status: För information, åtgärd krävs',
            path: pipeline.getOutputDirPath() + '/nrb_infomangder_planering.xlsx',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb_infomangder_planering.xlsx')
        });
    }
});

pipeline.addStep({
    run: function(pipeline) {
        pipeline.writeFile({
            title: 'Leveransspecifikation - Dokument - Informationsmängder - Programhandling (PDF)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-dokument-programhandling-ph.pdf',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-dokument-programhandling-ph.pdf')
        });
        pipeline.writeFile({
            title: 'Leveransspecifikation - Dokument - Informationsmängder - Programhandling (MS Word)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-dokument-programhandling-ph.docx',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-dokument-programhandling-ph.docx')
        });
    }
});

pipeline.addStep({
    run: function(pipeline) {
        pipeline.writeFile({
            title: 'Leveransspecifikation - Dokument - Informationsmängder - Systemhandling (PDF)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-dokument-systemhandling-sh.pdf',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-dokument-systemhandling-sh.pdf')
        });
        pipeline.writeFile({
            title: 'Leveransspecifikation - Dokument - Informationsmängder - Systemhandling (MS Word)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-dokument-systemhandling-sh.docx',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-dokument-systemhandling-sh.docx')
        });
    }
});

pipeline.addStep({
    run: function(pipeline) {
        pipeline.writeFile({
            title: 'Leveransspecifikation - Dokument - Informationsmängder - Bygghandling (PDF)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-dokument-bygghandling-bh.pdf',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-dokument-bygghandling-bh.pdf')
        });
        pipeline.writeFile({
            title: 'Leveransspecifikation - Dokument - Informationsmängder - Bygghandling (MS Word)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-dokument-bygghandling-bh.docx',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-dokument-bygghandling-bh.docx')
        });
    }
});

pipeline.addStep({
    run: function(pipeline) {
        pipeline.writeFile({
            title: 'Leveransspecifikation - Dokument - Informationsmängder - Relationshandling (PDF)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-dokument-relationshandling-rh.pdf',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-dokument-relationshandling-rh.pdf')
        });
        pipeline.writeFile({
            title: 'Leveransspecifikation - Dokument - Informationsmängder - Relationshandling (MS Word)',
            description: 'Status: Fastställd',
            path: pipeline.getOutputDirPath() + '/nrb-levspec-dokument-relationshandling-rh.docx',
            data: fs.readFileSync(pipeline.getBasePath() + '/raw_files/nrb-levspec-dokument-relationshandling-rh.docx')
        });
    }
});

pipeline
    .run()
    .then((data) => {
        return pipeline.generateStaticPage();
    })
    .then(() => {
        console.log(pipeline.prettyTable());
    })
    .catch((err) => {
        console.error(err);
    });
